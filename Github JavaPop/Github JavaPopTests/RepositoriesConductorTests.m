//
//  RepositoriesConductorTests.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 09/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <XCTest/XCTest.h>

// Conductor
#import "RepositoriesConductor.h"

@interface RepositoriesConductorTests : XCTestCase

@end

@implementation RepositoriesConductorTests

-(void)setUp {
    
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
}

-(void)tearDown {
    
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
    [super tearDown];
    
}

-(void)testInstance {
    
    XCTAssert( [RepositoriesConductor new] );
    
}

@end
