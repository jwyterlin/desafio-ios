//
//  PropertyHelperTests.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 02/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <XCTest/XCTest.h>

// Util
#import "PropertyHelper.h"

@interface PropertyHelperTests : XCTestCase

@end

@implementation PropertyHelperTests

-(void)setUp {
 
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
}

-(void)tearDown {
 
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
    [super tearDown];
    
}

-(void)testInstance {
    
    XCTAssert( [PropertyHelper new] );
    
}

-(void)testCheckPropertyExists {
    
    XCTAssert( [PropertyHelper checkProperty:"count" forClass:[NSArray class] isEqualType:@"NSUInteger"] );
    
}

-(void)testCheckPropertyDoesNotExist {
    
    XCTAssertFalse( [PropertyHelper checkProperty:"unknownProperty" forClass:[NSObject class] isEqualType:@"NSUInteger"] );
    
}

@end
