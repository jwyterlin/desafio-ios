//
//  PullRequestServiceTests.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 02/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <XCTest/XCTest.h>

// Service
#import "PullRequestService.h"

@interface PullRequestServiceTests : XCTestCase

@end

@implementation PullRequestServiceTests

-(void)setUp {
 
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
}

-(void)tearDown {
 
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
    [super tearDown];
    
}

-(void)testInstance {
    
    XCTAssert( [PullRequestService new] );
    
}

-(void)testGetPullRequests {
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"Query timed out."];
    
    [[PullRequestService new] pullRequestsWithAuthor:@"specta" repositoryName:@"specta" page:@1 completion:^(NSArray<PullRequestModel *> *pullRequests) {
        
        XCTAssert( pullRequests );
        
        [expectation fulfill];
        
    } failure:^(BOOL hasNoConnection, NSError *error) {
        
        if ( hasNoConnection ) {
            XCTFail( @"No internet" );
            [expectation fulfill];
            return;
        }
        
        XCTFail( @"%@", error.localizedDescription );
        
        [expectation fulfill];
        
    } test:^(id responseData, NSError *error) {
        
        if ( error ) {
            XCTFail( @"%@", error.localizedDescription );
            [expectation fulfill];
            return;
        }
        
    }];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            XCTFail( @"Error: %@", error );
        }
    }];
    
}

@end
