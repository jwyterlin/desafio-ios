//
//  OwnerModelTests.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 02/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <XCTest/XCTest.h>

// Model
#import "OwnerModel.h"

// Util
#import "PropertyHelper.h"

@interface OwnerModelTests : XCTestCase

@end

@implementation OwnerModelTests

-(void)setUp {
 
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
}

-(void)tearDown {
 
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
    [super tearDown];
    
}

-(void)testInstance {
    
    XCTAssert( [OwnerModel new] );
    
}

-(void)testPropertyTypeUsername {
    
    XCTAssert( [PropertyHelper checkProperty:"username" forClass:[OwnerModel class] isEqualType:@"NSString"] );
    
}

-(void)testPropertyTypeAvatarUrl {
    
    XCTAssert( [PropertyHelper checkProperty:"avatarUrl" forClass:[OwnerModel class] isEqualType:@"NSString"] );
    
}

-(void)testPropertyTypeAvatar {
    
    XCTAssert( [PropertyHelper checkProperty:"avatar" forClass:[OwnerModel class] isEqualType:@"UIImage"] );
    
}

@end
