//
//  RoutesTests.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 02/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <XCTest/XCTest.h>

// Util
#import "Routes.h"

@interface RoutesTests : XCTestCase

@end

@implementation RoutesTests

-(void)setUp {
    
    [super setUp];
 
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
}

-(void)tearDown {
 
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
    [super tearDown];
    
}

-(void)testInstance {
    
    XCTAssert( [Routes new] );
    
}

-(void)testBaseUrl {
    
    XCTAssert( [Routes BASE_URL] );
    
}

-(void)testBaseUrlAPI {
    
    XCTAssert( [Routes BASE_URL_API] );
    
}

-(void)testWsSearchRepositories {
    
    XCTAssert( [Routes WS_SEARCH_REPOSITORIES] );
    
}

-(void)testBaseUrlIsRight {
    
    XCTAssert( [[Routes BASE_URL] isEqualToString:@"https://api.github.com/"] );
    
}

-(void)testBaseUrlAPIIsRight {
    
    XCTAssert( [[Routes BASE_URL_API] isEqualToString:@"https://api.github.com/"] );
    
}

-(void)testWsSearchRepositoriesIsRight {
    
    XCTAssert( [[Routes WS_SEARCH_REPOSITORIES] isEqualToString:@"search/repositories"] );
    
}

-(void)testWsPullRequestsByRepositoryIsRight {
    
    XCTAssert( [[Routes WS_PULL_REQUESTS_BY_REPOSITORYWithAuthor:@"specta" repositoryName:@"specta"] isEqualToString:@"repos/specta/specta/pulls"] );
    
}

-(void)testBaseUrlIsWrong {
    
    XCTAssertFalse( [[Routes BASE_URL] isEqualToString:@"https://api.bitbucket.com/"] );
    
}

-(void)testBaseUrlAPIIsWrong {
    
    XCTAssertFalse( [[Routes BASE_URL_API] isEqualToString:@"https://api.gitlab.com/"] );
    
}

-(void)testWsSearchRepositoriesIsWrong {
    
    XCTAssertFalse( [[Routes WS_SEARCH_REPOSITORIES] isEqualToString:@"repositories"] );
    
}

-(void)testWsPullRequestsByRepositoryIsWrong {
    
    XCTAssertFalse( [[Routes WS_PULL_REQUESTS_BY_REPOSITORYWithAuthor:@"specta" repositoryName:@"specta"] isEqualToString:@"repositories/specta/specta/pulls"] );
    
}

@end
