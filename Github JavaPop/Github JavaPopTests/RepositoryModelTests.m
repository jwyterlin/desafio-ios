//
//  RepositoryModelTests.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 01/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <XCTest/XCTest.h>

// Util
#import "PropertyHelper.h"

// Model
#import "RepositoryModel.h"

@interface RepositoryModelTests : XCTestCase

@end

@implementation RepositoryModelTests

-(void)setUp {
 
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
}

-(void)tearDown {
    
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
    [super tearDown];
    
}

-(void)testInstance {
    
    XCTAssert( [RepositoryModel new] );
    
}

-(void)testPropertyTypeName {
    
    XCTAssert( [PropertyHelper checkProperty:"name" forClass:[RepositoryModel class] isEqualType:@"NSString"] );
    
}

-(void)testPropertyTypeDetail {
    
    XCTAssert( [PropertyHelper checkProperty:"detail" forClass:[RepositoryModel class] isEqualType:@"NSString"] );
    
}

-(void)testPropertyTypeNumberOfForks {
    
    XCTAssert( [PropertyHelper checkProperty:"numberOfForks" forClass:[RepositoryModel class] isEqualType:@"NSNumber"] );
    
}

-(void)testPropertyTypeNumberOfStars {
    
    XCTAssert( [PropertyHelper checkProperty:"numberOfStars" forClass:[RepositoryModel class] isEqualType:@"NSNumber"] );
    
}

-(void)testPropertyTypeOwner {
    
    XCTAssert( [PropertyHelper checkProperty:"owner" forClass:[RepositoryModel class] isEqualType:@"OwnerModel"] );
    
}

@end
