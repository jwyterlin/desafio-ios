//
//  ConstantsTests.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 01/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <XCTest/XCTest.h>

// Util
#import "Constants.h"

@interface ConstantsTests : XCTestCase

@end

@implementation ConstantsTests

-(void)setUp {
    
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
}

-(void)tearDown {
    
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
    [super tearDown];
    
}

-(void)testkNibNameRepositoryCell {
    
    XCTAssert( [kNibNameRepositoryCell isEqualToString:@"RepositoryCell"] );
    
}

-(void)testkNibNamePullRequestCell {
    
    XCTAssert( [kNibNamePullRequestCell isEqualToString:@"PullRequestCell"] );
    
}

-(void)testkNibNameLoadingMoreCell {
    
    XCTAssert( [kNibNameLoadingMoreCell isEqualToString:@"LoadingMoreCell"] );
    
}

-(void)testRepositoryServiceErrorDomain {
    
    XCTAssert( [RepositoryServiceErrorDomain isEqualToString:@"RepositoryServiceErrorDomain"] );
    
}

-(void)testPullRequestServiceErrorDomain {
    
    XCTAssert( [PullRequestServiceErrorDomain isEqualToString:@"PullRequestServiceErrorDomain"] );
    
}

-(void)testRepositoryServiceErrorInvalidRepositoriesJSONDictionary {
    
    XCTAssert( RepositoryServiceErrorInvalidRepositoriesJSONDictionary == 1 );
    
}

-(void)testPullRequestServiceErrorInvalidRepositoriesJSONDictionary {
    
    XCTAssert( PullRequestServiceErrorInvalidRepositoriesJSONDictionary == 1 );
    
}

-(void)testLimitPullRequestsPerPage {
    
    XCTAssert( LimitPullRequestsPerPage == 30 );
    
}

-(void)testkNotificationApplicationDidBecomeActive {
    
    XCTAssert( [kNotificationApplicationDidBecomeActive isEqualToString:@"NotificationApplicationDidBecomeActive"] );
    
}

@end
