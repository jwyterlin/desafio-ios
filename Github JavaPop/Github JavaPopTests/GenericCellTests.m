//
//  GenericCellTests.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 02/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <XCTest/XCTest.h>

// View
#import "GenericCell.h"

@interface GenericCellTests : XCTestCase

@end

@implementation GenericCellTests

-(void)setUp {
 
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
}

-(void)tearDown {
    
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
    [super tearDown];
    
}

-(void)testInstance {
    
    XCTAssert( [GenericCell new] );
    
}

-(void)testHeightForCellIsStandard {
    
    XCTAssert( [GenericCell heightForCell] == 44.0f );
    
}

-(void)testHeightForCellIsNotStandard {
    
    XCTAssertFalse( [GenericCell heightForCell] == 10.0f );
    
}

@end
