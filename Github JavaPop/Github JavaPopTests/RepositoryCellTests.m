//
//  RepositoryCellTests.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 02/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <XCTest/XCTest.h>

// View
#import "RepositoryCell.h"

@interface RepositoryCellTests : XCTestCase

@end

@implementation RepositoryCellTests

-(void)setUp {
 
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
}

-(void)tearDown {
 
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
    [super tearDown];
    
}

-(void)testInstance {
    
    XCTAssert( [RepositoryCell new] );
    
}

-(void)testHeightForCellIsStandard {
    
    XCTAssert( [RepositoryCell heightForCell] == 127.0f );
    
}

-(void)testHeightForCellIsNotStandard {
    
    XCTAssertFalse( [RepositoryCell heightForCell] == 100.0f );
    
}

@end
