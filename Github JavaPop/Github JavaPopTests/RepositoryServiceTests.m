//
//  RepositoryServiceTests.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 01/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <XCTest/XCTest.h>

// Service
#import "RepositoryService.h"

@interface RepositoryServiceTests : XCTestCase

@end

@implementation RepositoryServiceTests

-(void)setUp {
    
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
}

-(void)tearDown {
    
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
    [super tearDown];
    
}

-(void)testInstance {
    
    XCTAssert( [RepositoryService new] );
    
}

-(void)testGetRepositories {
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"Query timed out."];
    
    [[RepositoryService new] repositoriesWithPage:@1 completion:^(NSArray<RepositoryModel *> *repositories) {
        
        XCTAssert( repositories );
        
        XCTAssert( repositories.count > 0 );
        
        for ( RepositoryModel *repository in repositories ) {
            
            XCTAssert( repository.name );
            XCTAssert( repository.detail );
            XCTAssert( repository.numberOfForks );
            XCTAssert( repository.numberOfStars );
            XCTAssert( repository.owner );
            
        }
        
        [expectation fulfill];
        
    } failure:^(BOOL hasNoConnection, NSError *error) {
        
        if ( hasNoConnection ) {
            XCTFail( @"No internet" );
            [expectation fulfill];
            return;
        }
        
        XCTFail( @"%@", error.localizedDescription );
        
        [expectation fulfill];
        
    } test:^(id responseData, NSError *error) {
        
        if ( error ) {
            XCTFail( @"%@", error.localizedDescription );
            [expectation fulfill];
            return;
        }
        
        NSDictionary *result = (NSDictionary *)responseData;
        
        XCTAssert( result[@"items"] );
        
        NSArray *items = result[@"items"];
        
        for ( NSDictionary *item in items ) {
            
            XCTAssert( item[@"name"] );
            XCTAssert( item[@"description"] );
            XCTAssert( item[@"forks_count"] );
            XCTAssert( item[@"stargazers_count"] );
            XCTAssert( item[@"owner"] );
            
            NSDictionary *owner = item[@"owner"];
            
            XCTAssert( owner[@"login"] );
            XCTAssert( owner[@"avatar_url"] );
            
        }
        
    }];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            XCTFail( @"Error: %@", error );
        }
    }];
    
}

@end
