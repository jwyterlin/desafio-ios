//
//  PullRequestModelTests.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 02/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <XCTest/XCTest.h>

// Model
#import "PullRequestModel.h"

// Util
#import "PropertyHelper.h"

@interface PullRequestModelTests : XCTestCase

@end

@implementation PullRequestModelTests

-(void)setUp {
    
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
}

-(void)tearDown {
    
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
    [super tearDown];
    
}

-(void)testInstance {
    
    XCTAssert( [PullRequestModel new] );
    
}

-(void)testPropertyTypeTitle {
    
    XCTAssert( [PropertyHelper checkProperty:"title" forClass:[PullRequestModel class] isEqualType:@"NSString"] );
    
}

-(void)testPropertyTypeBody {
    
    XCTAssert( [PropertyHelper checkProperty:"body" forClass:[PullRequestModel class] isEqualType:@"NSString"] );
    
}

-(void)testPropertyTypeDate {
    
    XCTAssert( [PropertyHelper checkProperty:"date" forClass:[PullRequestModel class] isEqualType:@"NSDate"] );
    
}

-(void)testPropertyTypeUrl {
    
    XCTAssert( [PropertyHelper checkProperty:"url" forClass:[PullRequestModel class] isEqualType:@"NSString"] );
    
}

-(void)testPropertyTypeAuthor {
    
    XCTAssert( [PropertyHelper checkProperty:"author" forClass:[PullRequestModel class] isEqualType:@"OwnerModel"] );
    
}

@end
