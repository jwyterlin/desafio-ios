//
//  AppDelegate.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 28/10/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

