//
//  UINavigationController+Helper.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 01/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "UINavigationController+Helper.h"

@implementation UINavigationController (Helper)

-(UIViewController *)childViewControllerForStatusBarStyle {
    return self.topViewController;
}

@end
