//
//  UIView+Helper.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 02/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "UIView+Helper.h"

// Util
#import "DeviceInfo.h"

@implementation UIView (Helper)

-(void)setX:(CGFloat)x {
    
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
    
}

-(void)setY:(CGFloat)y {
    
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
    
}

-(void)setWidth:(CGFloat)width {
    
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
    
}

-(void)setHeight:(CGFloat)height {
    
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
    
}

-(CGFloat)x {
    return self.frame.origin.x;
}

-(CGFloat)y {
    return self.frame.origin.y;
}

-(CGFloat)width {
    return self.frame.size.width;
}

-(CGFloat)height {
    return self.frame.size.height;
}

-(CGFloat)horizontalSpace {
    
    return self.x + self.width;
    
}

-(CGFloat)verticalSpace {
    
    return self.y + self.height;
    
}

-(void)changeLayoutConstraintWidthWithConstraintToRemove:(NSLayoutConstraint *)constraintToRemove {
    
    int totalWidth = [DeviceInfo width];
    int viewX = self.frame.origin.x;
    int newWidth = totalWidth - ( 2 * viewX );
    
    [self changeLayoutConstraintWidthWithConstraintToRemove:constraintToRemove newWidth:newWidth];
    
}

-(void)changeLayoutConstraintWidthWithConstraintToRemove:(NSLayoutConstraint *)constraintToRemove newWidth:(CGFloat)newWidth {
    
    NSLayoutConstraint *constraintWidth = [NSLayoutConstraint constraintWithItem:self
                                                                       attribute:NSLayoutAttributeWidth
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:nil
                                                                       attribute:NSLayoutAttributeWidth
                                                                      multiplier:1
                                                                        constant:newWidth];
    
    [self removeConstraint:constraintToRemove];
    [self addConstraint:constraintWidth];
    [self setNeedsUpdateConstraints];
    
}

@end
