//
//  UITableView+Helper.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 04/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "UITableView+Helper.h"

@implementation UITableView (Helper)

-(void)registerNibsForCellReuseIdentifiers:(NSArray *)identifiers {
    
    if ( ! identifiers )
        return;
    
    if ( identifiers.count == 0 )
        return;
    
    for ( NSString *identifier in identifiers )
        [self registerNibForCellReuseIdentifier:identifier];
    
}

-(void)registerNibForCellReuseIdentifier:(NSString *)identifier {
    
    UINib *nib = [UINib nibWithNibName:identifier bundle:nil];
    [self registerNib:nib forCellReuseIdentifier:identifier];
    
}

-(void)removeSeparator {
    self.separatorColor = [UIColor clearColor];
}

-(void)restoreSeparator {
    self.separatorColor = [UIColor colorWithRed:0.783922f green:0.780392f blue:0.8f alpha:1.0f];
}

@end
