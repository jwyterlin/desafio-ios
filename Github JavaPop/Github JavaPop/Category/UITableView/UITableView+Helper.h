//
//  UITableView+Helper.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 04/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (Helper)

-(void)registerNibsForCellReuseIdentifiers:(NSArray *)identifiers;

-(void)registerNibForCellReuseIdentifier:(NSString *)identifier;

-(void)removeSeparator;

-(void)restoreSeparator;

@end
