//
//  UIViewController+NoData.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 11/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "UIViewController+NoData.h"

#import <objc/runtime.h>

// Util
#import "DeviceInfo.h"

// Category
#import "UIView+Helper.h"

@interface UIViewController()

@property(nonatomic,assign) UILabel *noDataLabel;

@end

static char kNoDataLabelKey;

@implementation UIViewController (NoData)

#pragma mark - Public methods

-(void)showNoDataLabelWithText:(NSString *)text {
    
    if ( ! self.noDataLabel )
        [self createNoDataLabel];
    
    self.noDataLabel.text = text;
    
    [self.view addSubview:self.noDataLabel];
    [self.view bringSubviewToFront:self.noDataLabel];
    
}

-(void)hideNoDataLabel {
    
    if ( ! self.noDataLabel )
        [self createNoDataLabel];
    
    [self.noDataLabel removeFromSuperview];
    
}

#pragma mark - Private methods

-(void)createNoDataLabel {
    
    CGFloat height = 21.0f;
    CGFloat y = self.view.height/2 - height/2;
    
    [self setNoDataLabel:[[UILabel alloc] initWithFrame:CGRectMake( 0, y, [DeviceInfo width], height )]];
    self.noDataLabel.backgroundColor = [UIColor clearColor];
    self.noDataLabel.text = @"";
    self.noDataLabel.textColor = [UIColor darkGrayColor];
    self.noDataLabel.textAlignment = NSTextAlignmentCenter;
    
}

#pragma mark - Creating instances variables

// Property: noDataLabel
-(void)setNoDataLabel:(UILabel *)noDataLabel {
    objc_setAssociatedObject(self, &kNoDataLabelKey, noDataLabel, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(UILabel *)noDataLabel {
    return objc_getAssociatedObject(self, &kNoDataLabelKey);
}

@end
