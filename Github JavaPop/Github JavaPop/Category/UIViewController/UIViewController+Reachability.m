//
//  UIViewController+Reachability.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 09/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "UIViewController+Reachability.h"

#import <objc/runtime.h>

// Category
#import "Reachability+Helper.h"
#import "UIView+Helper.h"

// Utils
#import "DeviceInfo.h"

@interface UIViewController()

@property(nonatomic,assign) UIView *noConnectionView;

@end

static char kNoConnectionViewKey;

@implementation UIViewController (Reachability)

#pragma mark - Public methods

-(void)observeReachability {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(listenReachabilityChangedNotification:) name:kReachabilityChangedNotification object:nil];
    
}

-(void)checkConnection {
    
    if ( [Reachability isNetworkReachable] ) {
        [self hideNoConnection];
    } else {
        [self showNoConnection];
    }
    
}

#pragma mark - Notification methods

-(void)listenReachabilityChangedNotification:(NSNotification *)notification {
    
    [self checkConnection];
    
}

#pragma mark - Private methods

-(void)showNoConnection {
    
    if ( ! self.noConnectionView )
        [self createNoConnectionView];
    
    [self.view addSubview:self.noConnectionView];
    [self.view bringSubviewToFront:self.noConnectionView];
    
    self.noConnectionView.y = -self.noConnectionView.height;
    
    [UIView animateWithDuration:0.7f animations:^{
        self.noConnectionView.y = 0.0f;
    }];
    
}

-(void)hideNoConnection {
    
    if ( ! self.noConnectionView )
        [self createNoConnectionView];
    
    [UIView animateWithDuration:0.7f animations:^{
        self.noConnectionView.y = -self.noConnectionView.height;
    } completion:^(BOOL finished) {
        [self.noConnectionView removeFromSuperview];
    }];
    
}

-(void)createNoConnectionView {
    
    [self setNoConnectionView:[[UIView alloc] initWithFrame:CGRectMake(0, 0, [DeviceInfo width], 21.0f )]];
    self.noConnectionView.backgroundColor = [UIColor colorWithWhite:0.9f alpha:1.0f];
    
    UILabel *noConnectionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [DeviceInfo width], 21.0f)];
    noConnectionLabel.text = NSLocalizedString(@"No connection", nil);
    noConnectionLabel.textColor = [UIColor redColor];
    noConnectionLabel.textAlignment = NSTextAlignmentCenter;
    
    [self.noConnectionView addSubview:noConnectionLabel];
    
}

#pragma mark - Creating instances variables

// Property: noConnectionView
-(void)setNoConnectionView:(UIView *)noConnectionView {
    objc_setAssociatedObject(self, &kNoConnectionViewKey, noConnectionView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(UIView *)noConnectionView {
    return objc_getAssociatedObject(self, &kNoConnectionViewKey);
}

@end
