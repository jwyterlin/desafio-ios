//
//  UIViewController+NoData.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 11/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (NoData)

-(void)showNoDataLabelWithText:(NSString *)text;

-(void)hideNoDataLabel;

@end
