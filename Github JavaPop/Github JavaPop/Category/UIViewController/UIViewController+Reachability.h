//
//  UIViewController+Reachability.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 09/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Reachability)

-(void)observeReachability;

-(void)checkConnection;

@end
