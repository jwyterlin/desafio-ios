//
//  NSAttributedString+Helper.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 05/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <Foundation/Foundation.h>

// Category
#import "NSString+FontAwesome.h"

@interface NSAttributedString (Helper)

-(NSAttributedString *)attributedStringWithIcon:(FAIcon)icon quantity:(NSNumber *)quantity;

@end
