//
//  NSAttributedString+Helper.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 05/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "NSAttributedString+Helper.h"

// Category
#import "UIFont+FontAwesome.h"

@implementation NSAttributedString (Helper)

-(NSAttributedString *)attributedStringWithIcon:(FAIcon)icon quantity:(NSNumber *)quantity {
    
    NSString *quantityString = [quantity stringValue];
    
    NSString *iconString = [NSString fontAwesomeIconStringForEnum:icon];
    
    NSString *fullText = [NSString stringWithFormat:@"%@ %@", iconString, quantityString];
    
    NSString *attributeName = NSFontAttributeName;
    UIFont *fontAwesome = [UIFont fontAwesomeFontOfSize:17.0f];
    UIFont *fontSystem = [UIFont systemFontOfSize:17.0f];
    
    NSRange rangeIcon = NSMakeRange( 0, iconString.length );
    NSRange rangeQuantity = NSMakeRange( iconString.length, fullText.length - iconString.length );
    
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:fullText];
    [mutableAttributedString addAttribute:attributeName value:fontAwesome range:rangeIcon];
    [mutableAttributedString addAttribute:attributeName value:fontSystem range:rangeQuantity];
    
    return mutableAttributedString;
    
}

@end
