//
//  RepositoryModel.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 01/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "RepositoryModel.h"

@implementation RepositoryModel

+(NSDictionary *)JSONKeyPathsByPropertyKey {
    
    return @{
             @"name": @"name",
             @"detail": @"description",
             @"numberOfForks":@"forks_count",
             @"numberOfStars":@"stargazers_count",
             @"owner":@"owner"
            };
    
}

@end
