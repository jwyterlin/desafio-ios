//
//  PullRequestModel.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 02/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "PullRequestModel.h"

@implementation PullRequestModel

+(NSDictionary *)JSONKeyPathsByPropertyKey {
    
    return @{
             @"title": @"title",
             @"body": @"body",
             @"date": @"created_at",
             @"url": @"html_url",
             @"author": @"user"
            };
    
}

+(NSValueTransformer *)dateJSONTransformer {
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ssZZZ"];
    
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *dateString, BOOL *success, NSError *__autoreleasing *error) {
        return [dateFormatter dateFromString:dateString];
    } reverseBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        return [dateFormatter stringFromDate:value];
    }];
    
}

@end
