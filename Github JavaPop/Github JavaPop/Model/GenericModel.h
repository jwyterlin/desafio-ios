//
//  GenericModel.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 01/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface GenericModel : MTLModel<MTLJSONSerializing>

@end
