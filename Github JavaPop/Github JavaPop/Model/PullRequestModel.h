//
//  PullRequestModel.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 02/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "GenericModel.h"

// Model
#import "OwnerModel.h"

@interface PullRequestModel : GenericModel

@property(nonatomic, copy, readonly) NSString *title;
@property(nonatomic, copy, readonly) NSString *body;
@property(nonatomic, copy, readonly) NSDate *date;
@property(nonatomic, copy, readonly) NSString *url;
@property(nonatomic, copy, readonly) OwnerModel *author;

@end
