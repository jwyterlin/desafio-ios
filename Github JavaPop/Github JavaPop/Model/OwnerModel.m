//
//  OwnerModel.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 02/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "OwnerModel.h"

@implementation OwnerModel

+(NSDictionary *)JSONKeyPathsByPropertyKey {
    
    return @{
             @"username": @"login",
             @"avatarUrl": @"avatar_url"
            };
    
}

@end
