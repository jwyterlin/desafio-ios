//
//  OwnerModel.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 02/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "GenericModel.h"

#import <UIKit/UIImage.h>

@interface OwnerModel : GenericModel

@property(nonatomic, copy, readonly) NSString *username;
@property(nonatomic, copy, readonly) NSString *avatarUrl;
@property(nonatomic) UIImage *avatar;

@end
