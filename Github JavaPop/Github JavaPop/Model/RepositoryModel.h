//
//  RepositoryModel.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 01/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "GenericModel.h"

// Model
#import "OwnerModel.h"

@interface RepositoryModel : GenericModel

@property(nonatomic, copy, readonly) NSString *name;
@property(nonatomic, copy, readonly) NSString *detail;
@property(nonatomic, copy, readonly) NSNumber *numberOfForks;
@property(nonatomic, copy, readonly) NSNumber *numberOfStars;
@property(nonatomic, copy, readonly) OwnerModel *owner;

@end
