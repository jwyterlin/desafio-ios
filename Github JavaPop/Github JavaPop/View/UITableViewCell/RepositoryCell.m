//
//  RepositoryCell.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 01/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "RepositoryCell.h"

// Category
#import "NSAttributedString+Helper.h"

// Pods
#import "UIImageView+AFNetworking.h"

@interface RepositoryCell()

@property(nonatomic,weak) IBOutlet UILabel *repositoryNameLabel;
@property(nonatomic,weak) IBOutlet UILabel *repositoryDescriptionLabel;
@property(nonatomic,weak) IBOutlet UILabel *forksLabel;
@property(nonatomic,weak) IBOutlet UILabel *starsLabel;
@property(nonatomic,weak) IBOutlet UIImageView *userPhotoImageView;
@property(nonatomic,weak) IBOutlet UILabel *usernameLabel;
@property(nonatomic,weak) IBOutlet UILabel *nameSurenameLabel;

@property(nonatomic,strong) NSAttributedString *awesomeAttributedString;

@end

@implementation RepositoryCell

#pragma mark - Public methods

+(RepositoryCell *)cellAtIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView repository:(RepositoryModel *)repository {
    
    RepositoryCell *cell = [tableView dequeueReusableCellWithIdentifier:kNibNameRepositoryCell forIndexPath:indexPath];
    
    [self configureCell:cell indexPath:indexPath repository:repository];
    
    return cell;
    
}

+(void)configureCell:(RepositoryCell *)cell indexPath:(NSIndexPath *)indexPath repository:(RepositoryModel *)repository {
    
    // Repository name
    cell.repositoryNameLabel.text = repository.name;
    
    // Repository description
    cell.repositoryDescriptionLabel.text = repository.detail;
    
    // Number of Forks
    cell.forksLabel.attributedText = [cell.awesomeAttributedString attributedStringWithIcon:FACodeFork quantity:repository.numberOfForks];

    // Number of Stars
    cell.starsLabel.attributedText = [cell.awesomeAttributedString attributedStringWithIcon:FAStar quantity:repository.numberOfStars];
    
    // Owner's username
    cell.usernameLabel.text = repository.owner.username;
    
    // Owner's photo
    cell.userPhotoImageView.layer.masksToBounds = YES;
    cell.userPhotoImageView.layer.cornerRadius = cell.userPhotoImageView.width/2;
    
    if ( repository.owner.avatar ) {
        cell.userPhotoImageView.image = repository.owner.avatar;
    } else {
        [cell setImageWithImageUrl:repository.owner.avatarUrl completion:^(UIImage *image) {
            repository.owner.avatar = image;
        }];
    }
    
    // Owner's name and surename
    cell.nameSurenameLabel.text = @"";
    
}

#pragma mark - Private methods

-(void)setImageWithImageUrl:(NSString *)imageUrl completion:(void(^)(UIImage *image))completion {
    
    NSURL *url = [NSURL URLWithString:imageUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    self.userPhotoImageView.image = nil;
    
    __weak RepositoryCell *weakCell = self;
    
    [self.userPhotoImageView setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        
        if ( completion )
            completion( image );
        
        weakCell.userPhotoImageView.image = image;
        [weakCell setNeedsLayout];
        
    } failure:nil];
    
}

#pragma mark - Overriding super methods

+(CGFloat)heightForCell {
    return 127.0f;
}

#pragma mark - Lazy Instances

-(NSAttributedString *)awesomeAttributedString {
    
    if ( ! _awesomeAttributedString ) {
        _awesomeAttributedString = [NSAttributedString new];
    }
    return _awesomeAttributedString;
    
}

@end
