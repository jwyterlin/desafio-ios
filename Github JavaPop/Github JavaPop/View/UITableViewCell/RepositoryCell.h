//
//  RepositoryCell.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 01/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "GenericCell.h"

// Model
#import "RepositoryModel.h"

@interface RepositoryCell : GenericCell

+(RepositoryCell *)cellAtIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView repository:(RepositoryModel *)repository;

+(void)configureCell:(RepositoryCell *)cell indexPath:(NSIndexPath *)indexPath repository:(RepositoryModel *)repository;

@end
