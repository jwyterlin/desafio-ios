//
//  PullRequestCell.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 09/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "GenericCell.h"

// Model
#import "PullRequestModel.h"

@interface PullRequestCell : GenericCell

+(PullRequestCell *)cellAtIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView pullRequest:(PullRequestModel *)pullRequest;

+(void)configureCell:(PullRequestCell *)cell indexPath:(NSIndexPath *)indexPath pullRequest:(PullRequestModel *)pullRequest;

@end
