//
//  GenericCell.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 01/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "GenericCell.h"

@implementation GenericCell

#pragma mark - Public methods

+(CGFloat)heightForCell {
    return 44.0f;
}

@end
