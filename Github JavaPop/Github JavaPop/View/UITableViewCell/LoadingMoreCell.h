//
//  LoadingMoreCell.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 09/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "GenericCell.h"

@interface LoadingMoreCell : GenericCell

+(UITableViewCell *)cellWithTableView:(UITableView *)tableView text:(NSString *)text completion:(void(^)())completion;

@end
