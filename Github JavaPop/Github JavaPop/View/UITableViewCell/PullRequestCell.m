//
//  PullRequestCell.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 09/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "PullRequestCell.h"

// Pods
#import "UIImageView+AFNetworking.h"

@interface PullRequestCell()

@property(nonatomic,weak) IBOutlet UILabel *titleLabel;
@property(nonatomic,weak) IBOutlet UILabel *bodyLabel;
@property(nonatomic,weak) IBOutlet UILabel *dateLabel;
@property(nonatomic,weak) IBOutlet UIImageView *userPhotoImageView;
@property(nonatomic,weak) IBOutlet UILabel *usernameLabel;
@property(nonatomic,weak) IBOutlet UILabel *nameSurenameLabel;

@end

@implementation PullRequestCell

#pragma mark - Public methods

+(PullRequestCell *)cellAtIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView pullRequest:(PullRequestModel *)pullRequest {
    
    PullRequestCell *cell = [tableView dequeueReusableCellWithIdentifier:kNibNamePullRequestCell forIndexPath:indexPath];
    
    [self configureCell:cell indexPath:indexPath pullRequest:pullRequest];
    
    return cell;
    
}

+(void)configureCell:(PullRequestCell *)cell indexPath:(NSIndexPath *)indexPath pullRequest:(PullRequestModel *)pullRequest {
    
    // Title
    cell.titleLabel.text = pullRequest.title;
    
    // Body
    cell.bodyLabel.text = pullRequest.body;
    
    // Date
    NSString *localizedDate = [NSDateFormatter localizedStringFromDate:pullRequest.date dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle];

    cell.dateLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Posted on %@", nil), localizedDate];
    
    // Author's username
    cell.usernameLabel.text = pullRequest.author.username;
    
    // Author's photo
    cell.userPhotoImageView.layer.masksToBounds = YES;
    cell.userPhotoImageView.layer.cornerRadius = cell.userPhotoImageView.width/2;
    
    if ( pullRequest.author.avatar ) {
        cell.userPhotoImageView.image = pullRequest.author.avatar;
    } else {
        [cell setImageWithImageUrl:pullRequest.author.avatarUrl completion:^(UIImage *image) {
            pullRequest.author.avatar = image;
        }];
    }
    
    // Author's name and surename
    cell.nameSurenameLabel.text = @"";
    
}

#pragma mark - Private methods

-(void)setImageWithImageUrl:(NSString *)imageUrl completion:(void(^)(UIImage *image))completion {
    
    NSURL *url = [NSURL URLWithString:imageUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    self.userPhotoImageView.image = nil;
    
    __weak PullRequestCell *weakCell = self;
    
    [self.userPhotoImageView setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        
        if ( completion )
            completion( image );
        
        weakCell.userPhotoImageView.image = image;
        [weakCell setNeedsLayout];
        
    } failure:nil];
    
}

#pragma mark - Overriding super methods

+(CGFloat)heightForCell {
    return 154.0f;
}

@end
