//
//  LoadingMoreCell.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 09/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "LoadingMoreCell.h"

@implementation LoadingMoreCell

+(UITableViewCell *)cellWithTableView:(UITableView *)tableView text:(NSString *)text completion:(void(^)())completion {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kNibNameLoadingMoreCell];
    
    if ( cell == nil )
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kNibNameLoadingMoreCell];
    
    cell.textLabel.text = text;
    cell.textLabel.textColor = [UIColor grayColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIActivityIndicatorView *loading = [cell.contentView viewWithTag:1];
    
    if ( ! loading ) {
        
        loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        loading.x = [DeviceInfo width] - loading.width - 16.0f;
        loading.y = 13.0f;
        loading.hidesWhenStopped = YES;
        loading.tag = 1;
        
        [cell.contentView addSubview:loading];
        
    }
    
    [loading startAnimating];
    
    if ( completion )
        completion();
    
    return cell;
    
}

@end
