//
//  GenericCell.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 01/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <UIKit/UIKit.h>

// Util
#import "Constants.h"
#import "DeviceInfo.h"

// Category
#import "UIView+Helper.h"

@interface GenericCell : UITableViewCell

+(CGFloat)heightForCell;

@end
