//
//  PullRequestsViewController.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 09/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "GenericViewController.h"

@interface PullRequestsViewController : GenericViewController

-(void)setAuthor:(NSString *)author;
-(void)setRepositoryName:(NSString *)repositoryName;

@end
