//
//  RepositoriesViewController.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 01/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "GenericViewController.h"

@interface RepositoriesViewController : GenericViewController

@end
