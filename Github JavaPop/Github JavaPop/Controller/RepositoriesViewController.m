//
//  RepositoriesViewController.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 01/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "RepositoriesViewController.h"

// Cell
#import "LoadingMoreCell.h"
#import "RepositoryCell.h"

// Conductor
#import "RepositoriesConductor.h"

// Category
#import "UITableView+Helper.h"
#import "UIViewController+NoData.h"
#import "UIViewController+Reachability.h"

// Controller
#import "PullRequestsViewController.h"

// View
#import "Indicator.h"

// Util
#import "Alert.h"

@interface RepositoriesViewController()<UITableViewDataSource,UITableViewDelegate,ListConductorDelegate>

@property(nonatomic,weak) IBOutlet UITableView *tableView;
@property(nonatomic,strong) UIRefreshControl *refreshControl;

@property(nonatomic,strong) RepositoriesConductor *repositoriesConductor;

@end

@implementation RepositoriesViewController

#pragma mark - View Lifecycle

-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self observeReachability];
    
    [self setupTableView];

}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.navigationItem.title = NSLocalizedString(@"Github JavaPop", nil);
    
    [self.repositoriesConductor viewWillAppear];
    
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
    
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    self.navigationItem.title = @"";
    
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

#pragma mark - Prepare for Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ( [[segue identifier] isEqualToString:@"PullRequestsViewController"] ) {
        
        PullRequestsViewController *pullRequestsViewController = [segue destinationViewController];
        
        RepositoryModel *repository = (RepositoryModel *)sender;
        
        [pullRequestsViewController setAuthor:repository.owner.username];
        [pullRequestsViewController setRepositoryName:repository.name];
        
    }

}

#pragma mark - IBAction methods

-(IBAction)refresh:(id)sender {
    
    [self.repositoriesConductor refresh];
    
}

#pragma mark - UITableViewDataSource methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger numberOfRows = [self.repositoriesConductor numberOfElements];
    
    return numberOfRows;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger numberOfRows = [self.repositoriesConductor numberOfElements];
    
    BOOL needsLoadMore = [self.repositoriesConductor needsLoadMore];
    
    if ( indexPath.row == numberOfRows - 1 && needsLoadMore ) {
        
        return [LoadingMoreCell cellWithTableView:tableView
                                             text:NSLocalizedString(@"Loading more repositories...", nil)
                                       completion:^{
                                           
           [self.repositoriesConductor downloadElements];
                                           
        }];
        
    }
    
    RepositoryModel *repository = [self.repositoriesConductor elementAtIndex:indexPath.row];
    
    return [RepositoryCell cellAtIndexPath:indexPath tableView:tableView repository:repository];
    
}

#pragma mark - UITableViewDelegate methods

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    RepositoryModel *repository = [self.repositoriesConductor elementAtIndex:indexPath.row];
    
    [self performSegueWithIdentifier:@"PullRequestsViewController" sender:repository];
    
}

#pragma mark - ListConductorDelegate methods

-(void)listConductor_reloadList {
    [self.tableView reloadData];
}

-(void)listConductor_showIndicator {
    [[Indicator shared] showIndicatorWithViewController:self];
}

-(void)listConductor_stopIndicator {
    [[Indicator shared] stopIndicatorInViewController:self];
    [self.refreshControl endRefreshing];
}

-(void)listConductor_hasNoConnection {
    [self checkConnection];
}

-(void)listConductor_handleError:(NSError *)error {
    [[Alert new] showError:error viewController:self];
}

-(void)listConductor_showNoData {
    [self showNoDataLabelWithText:NSLocalizedString(@"No repository found", nil)];
}

-(void)listConductor_hideNoData {
    
    [self hideNoDataLabel];
    [self.tableView restoreSeparator];
    
}

#pragma mark - Private methods

-(void)setupTableView {
    
    [self.tableView registerNibForCellReuseIdentifier:kNibNameRepositoryCell];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = [RepositoryCell heightForCell];
    self.tableView.backgroundColor = [UIColor colorWithWhite:0.85f alpha:1.0f];
    [self.tableView removeSeparator];
    
    self.refreshControl = [UIRefreshControl new];
    [self.refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    
}

#pragma mark - Lazy Instances

-(RepositoriesConductor *)repositoriesConductor {
    
    if ( ! _repositoriesConductor ) {
        _repositoriesConductor = [RepositoriesConductor new];
        _repositoriesConductor.delegate = self;
    }
    
    return _repositoriesConductor;
    
}

@end
