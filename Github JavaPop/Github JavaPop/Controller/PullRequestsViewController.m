//
//  PullRequestsViewController.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 09/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "PullRequestsViewController.h"

// Conductor
#import "PullRequestsConductor.h"

// Cell
#import "LoadingMoreCell.h"
#import "PullRequestCell.h"

// Category
#import "UITableView+Helper.h"
#import "UIViewController+NoData.h"
#import "UIViewController+Reachability.h"

// View
#import "Indicator.h"

// Util
#import "Alert.h"

@interface PullRequestsViewController()<UITableViewDataSource,UITableViewDelegate,ListConductorDelegate>

// UI
@property(nonatomic,weak) IBOutlet UITableView *tableView;
@property(nonatomic,strong) UIRefreshControl *refreshControl;

// Conductor
@property(nonatomic,strong) PullRequestsConductor *pullRequestsConductor;

@end

@implementation PullRequestsViewController

#pragma mark - Public methods

-(void)setAuthor:(NSString *)author {
    [self.pullRequestsConductor setAuthor:author];
}

-(void)setRepositoryName:(NSString *)repositoryName {
    [self.pullRequestsConductor setRepositoryName:repositoryName];
}

#pragma mark - View Lifecycle

-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.navigationItem.title = self.pullRequestsConductor.repositoryName;
    
    [self observeReachability];
    
    [self setupTableView];
    
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.pullRequestsConductor viewWillAppear];
    
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

#pragma mark - IBAction methods

-(IBAction)refresh:(id)sender {
    
    [self.pullRequestsConductor refresh];
    
}

#pragma mark - UITableViewDataSource methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.pullRequestsConductor numberOfElements];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger numberOfRows = [self.pullRequestsConductor numberOfElements];
    
    if ( numberOfRows > LimitPullRequestsPerPage - 1 ) {
    
        BOOL needsLoadMore = [self.pullRequestsConductor needsLoadMore];
        
        if ( indexPath.row == numberOfRows - 1 && needsLoadMore ) {
            
            return [LoadingMoreCell cellWithTableView:tableView
                                                 text:NSLocalizedString(@"Loading more pull requests...", nil)
                                           completion:^{
                                               
                [self.pullRequestsConductor downloadElements];
                                               
            }];
            
        }
        
    }
    
    PullRequestModel *pullRequest = [self.pullRequestsConductor elementAtIndex:indexPath.row];
    
    return [PullRequestCell cellAtIndexPath:indexPath tableView:tableView pullRequest:pullRequest];
    
}

#pragma mark - UITableViewDelegate methods

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PullRequestModel *pullRequest = [self.pullRequestsConductor elementAtIndex:indexPath.row];
    
    [self callBrowserWithUrl:pullRequest.url];

}

#pragma mark - ListConductorDelegate methods

-(void)listConductor_reloadList {
    [self.tableView reloadData];
}

-(void)listConductor_showIndicator {
    [[Indicator shared] showIndicatorWithViewController:self];
}

-(void)listConductor_stopIndicator {
    [[Indicator shared] stopIndicatorInViewController:self];
    [self.refreshControl endRefreshing];
}

-(void)listConductor_hasNoConnection {
    [self checkConnection];
}

-(void)listConductor_handleError:(NSError *)error {
    [[Alert new] showError:error viewController:self];
}

-(void)listConductor_showNoData {
    [self showNoDataLabelWithText:NSLocalizedString(@"No pull request found.", nil)];
}

-(void)listConductor_hideNoData {
    [self hideNoDataLabel];
}

#pragma mark - Notification methods

-(void)listenApplicationDidBecomeActive:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationApplicationDidBecomeActive object:nil];
    
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
    
}

#pragma mark - Private methods

-(void)setupTableView {
    
    [self.tableView registerNibForCellReuseIdentifier:kNibNamePullRequestCell];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = [PullRequestCell heightForCell];
    self.tableView.backgroundColor = [UIColor colorWithWhite:0.8f alpha:1.0f];
    
    self.refreshControl = [UIRefreshControl new];
    [self.refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    
}

-(void)callBrowserWithUrl:(NSString *)url {
    
    if ( ! url )
        return;
    
    if ( [url isEqualToString:@""] )
        return;
    
    NSURL *nsurl = [NSURL URLWithString:url];
    
    UIApplication *application = [UIApplication sharedApplication];
    
    if ( [application canOpenURL:nsurl] ) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(listenApplicationDidBecomeActive:) name:kNotificationApplicationDidBecomeActive object:nil];
        
        if ( [application respondsToSelector:@selector(openURL:options:completionHandler:)] ) {
            [application openURL:nsurl options:@{} completionHandler:^(BOOL success){}];
        } else {
            if ( [application respondsToSelector:@selector(openURL:)] ) {
                [application openURL:nsurl];
            }
        }
        
    }
    
}

#pragma mark - Lazy Instances

-(PullRequestsConductor *)pullRequestsConductor {
    
    if ( ! _pullRequestsConductor ) {
        _pullRequestsConductor = [PullRequestsConductor new];
        _pullRequestsConductor.delegate = self;
    }
    
    return _pullRequestsConductor;
    
}

@end
