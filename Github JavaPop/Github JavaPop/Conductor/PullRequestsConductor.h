//
//  PullRequestsConductor.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 09/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <Foundation/Foundation.h>

// Conductor
#import "ListConductor.h"

// Model
#import "PullRequestModel.h"

@interface PullRequestsConductor : ListConductor

@property(nonatomic,strong) NSString *repositoryName;

-(void)setAuthor:(NSString *)author;

@end
