//
//  ListConductor.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 11/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ListConductorDelegate;

@interface ListConductor : NSObject

@property(nonatomic,weak) id<ListConductorDelegate> delegate;

-(NSUInteger)numberOfElements;

-(id)elementAtIndex:(NSUInteger)index;

-(void)viewWillAppear;

-(void)downloadElements;

-(void)refresh;

-(BOOL)needsLoadMore;

-(void)downloadElementsWithPage:(NSNumber *)page completion:(void(^)(NSArray *listOfElements))completion failure:(void(^)(BOOL hasNoConnection, NSError *error))failure;

@end

@protocol ListConductorDelegate <NSObject>

-(void)listConductor_reloadList;
-(void)listConductor_showIndicator;
-(void)listConductor_stopIndicator;
-(void)listConductor_hasNoConnection;
-(void)listConductor_handleError:(NSError *)error;
-(void)listConductor_showNoData;
-(void)listConductor_hideNoData;

@end
