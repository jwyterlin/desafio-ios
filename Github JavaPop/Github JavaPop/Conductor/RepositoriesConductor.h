//
//  RepositoriesConductor.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 04/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <Foundation/Foundation.h>

// Conductor
#import "ListConductor.h"

// Model
#import "RepositoryModel.h"

@interface RepositoriesConductor : ListConductor

@end
