//
//  PullRequestsConductor.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 09/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "PullRequestsConductor.h"

// Service
#import "PullRequestService.h"

@interface PullRequestsConductor()

@property(nonatomic,strong) NSMutableArray *pullRequests;
@property(nonatomic,strong) NSNumber *page;
@property(nonatomic) BOOL isDownloadingPullRequests;
@property(nonatomic,strong) NSString *author;
@property(nonatomic) BOOL needsLoadMore;
@property(nonatomic) BOOL firstDownload;

@end

@implementation PullRequestsConductor

#pragma mark - Overriding super methods

-(void)downloadElementsWithPage:(NSNumber *)page
                     completion:(void (^)(NSArray *listOfElements))completion
                        failure:(void (^)(BOOL hasNoConnection, NSError *error))failure {
    
    [[PullRequestService new] pullRequestsWithAuthor:self.author
                                      repositoryName:self.repositoryName
                                                page:page
                                          completion:^(NSArray<PullRequestModel *> *pullRequests) {
        
        if ( completion )
            completion( pullRequests );
        
    } failure:^(BOOL hasNoConnection, NSError *error) {
        
        if ( failure )
            failure( hasNoConnection, error );
        
    }];
    
}

@end
