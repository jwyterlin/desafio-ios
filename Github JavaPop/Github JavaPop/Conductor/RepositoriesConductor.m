//
//  RepositoriesConductor.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 04/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "RepositoriesConductor.h"

// Service
#import "RepositoryService.h"

@interface RepositoriesConductor()

@end

@implementation RepositoriesConductor

-(void)downloadElementsWithPage:(NSNumber *)page
                     completion:(void (^)(NSArray *listOfElements))completion
                        failure:(void (^)(BOOL hasNoConnection, NSError *error))failure {
    
    [[RepositoryService new] repositoriesWithPage:page completion:^(NSArray<RepositoryModel *> *repositories) {
        
        if ( completion )
            completion( repositories );
        
    } failure:^(BOOL hasNoConnection, NSError *error) {
        
        if ( failure )
            failure( hasNoConnection, error );
        
    }];
    
}

@end
