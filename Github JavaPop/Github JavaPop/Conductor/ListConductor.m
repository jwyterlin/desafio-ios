//
//  ListConductor.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 11/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "ListConductor.h"

@interface ListConductor()

@property(nonatomic,strong) NSMutableArray *listOfElements;
@property(nonatomic,strong) NSNumber *page;
@property(nonatomic) BOOL isDownloading;
@property(nonatomic) BOOL needsLoadMore;
@property(nonatomic) BOOL firstDownload;

@end

@implementation ListConductor

#pragma mark - Lifecycle

-(instancetype)init {
    
    self = [super init];
    
    if (self) {
        self.needsLoadMore = YES;
        self.firstDownload = YES;
    }
    
    return self;
    
}

#pragma mark - Public methods

-(NSUInteger)numberOfElements {
    return self.listOfElements.count;
}

-(id)elementAtIndex:(NSUInteger)index {
    
    if ( index < self.listOfElements.count ) {
        
        id element = self.listOfElements[index];
        return element;
        
    }
    
    return nil;
    
}

-(void)viewWillAppear {
    
    if ( self.listOfElements.count == 0 ) {
        self.page = @1;
        [self downloadElements];
    }
    
}

-(void)downloadElements {
    
    [self downloadElementsWithPage:self.page];
    
}

-(void)refresh {
    
    [self downloadElementsWithPage:@1];
    
}

-(void)downloadElementsWithPage:(NSNumber *)page completion:(void(^)(NSArray *listOfElements))completion failure:(void(^)(BOOL hasNoConnection, NSError *error))failure {}

#pragma mark - Private methods

-(void)downloadElementsWithPage:(NSNumber *)page {
    
    if ( self.isDownloading )
        return;
    
    self.isDownloading = YES;
    
    if ( self.firstDownload )
        [self showIndicator];

    void (^completionBlock)(NSArray *listOfElements) = ^(NSArray *listOfElements) {
        [self listReceivedSuccessfull:listOfElements page:page];
    };
    
    void (^failureBlock)(BOOL hasNoConnection, NSError *error) = ^(BOOL hasNoConnection, NSError *error) {
        [self failedWithError:error hasNoConnection:hasNoConnection];
    };

    [self downloadElementsWithPage:page completion:completionBlock failure:failureBlock];
    
}

-(void)listReceivedSuccessfull:(NSArray *)listReceived page:(NSNumber *)page {
    
    [self stopIndicator];
    self.isDownloading = NO;
    
    if ( self.firstDownload )
        self.firstDownload = NO;
    
    if ( listReceived.count > 0 ) {
        
        [self.listOfElements addObjectsFromArray:listReceived];
        self.page = [NSNumber numberWithInteger: [page integerValue] + 1 ];
        self.needsLoadMore = YES;
        
    } else {
        
        self.needsLoadMore = NO;
        
    }
    
    if ( [self numberOfElements] > 0 )
        [self hideNoData];
    else
        [self showNoData];
    
    [self reloadList];
    
}

-(void)failedWithError:(NSError *)error hasNoConnection:(BOOL)hasNoConnection {
    
    [self stopIndicator];
    self.isDownloading = NO;
    
    if ( hasNoConnection ) {
        [self hasNoConnection];
        return;
    }
    
    if ( error ) {
        [self handleError:error];
        return;
    }
    
}

#pragma mark - Delegate methods

-(void)reloadList {
    
    if ( self.delegate )
        if ( [self.delegate respondsToSelector:@selector(listConductor_reloadList)] )
            [self.delegate listConductor_reloadList];
    
}

-(void)showIndicator {
    
    if ( self.delegate )
        if ( [self.delegate respondsToSelector:@selector(listConductor_showIndicator)] )
            [self.delegate listConductor_showIndicator];
    
}

-(void)stopIndicator {
    
    if ( self.delegate )
        if ( [self.delegate respondsToSelector:@selector(listConductor_stopIndicator)] )
            [self.delegate listConductor_stopIndicator];
    
}

-(void)hasNoConnection {
    
    if ( self.delegate )
        if ( [self.delegate respondsToSelector:@selector(listConductor_hasNoConnection)] )
            [self.delegate listConductor_hasNoConnection];
    
}

-(void)handleError:(NSError *)error {
    
    if ( self.delegate )
        if ( [self.delegate respondsToSelector:@selector(listConductor_handleError:)] )
            [self.delegate listConductor_handleError:error];
    
}

-(void)showNoData {
    
    if ( self.delegate )
        if ( [self.delegate respondsToSelector:@selector(listConductor_showNoData)] )
            [self.delegate listConductor_showNoData];
    
}

-(void)hideNoData {
    
    if ( self.delegate )
        if ( [self.delegate respondsToSelector:@selector(listConductor_hideNoData)] )
            [self.delegate listConductor_hideNoData];
    
}

#pragma mark - Lazy Instances

-(NSMutableArray *)listOfElements {
    
    if ( ! _listOfElements )
        _listOfElements = [NSMutableArray new];
    return _listOfElements;
    
}

@end
