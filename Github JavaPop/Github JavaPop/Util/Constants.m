//
//  Constants.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 01/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "Constants.h"

@implementation Constants

// Cell Nib Names
NSString *const kNibNameRepositoryCell = @"RepositoryCell";
NSString *const kNibNamePullRequestCell = @"PullRequestCell";
NSString *const kNibNameLoadingMoreCell = @"LoadingMoreCell";

// Error Domain
NSString *const RepositoryServiceErrorDomain = @"RepositoryServiceErrorDomain";
NSString *const PullRequestServiceErrorDomain = @"PullRequestServiceErrorDomain";

// Repository Service Errors
const NSInteger RepositoryServiceErrorInvalidRepositoriesJSONDictionary = 1;

// Pull Request Service Errors
const NSInteger PullRequestServiceErrorInvalidRepositoriesJSONDictionary = 1;

const NSInteger LimitPullRequestsPerPage = 30;

// Notifications
NSString *const kNotificationApplicationDidBecomeActive = @"NotificationApplicationDidBecomeActive";

@end
