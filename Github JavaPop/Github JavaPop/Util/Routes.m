//
//  Routes.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 02/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "Routes.h"

@implementation Routes

+(NSString *)BASE_URL {
    return @"https://api.github.com/";
}

+(NSString *)BASE_URL_API {
    return [NSString stringWithFormat:@"%@",[Routes BASE_URL]];
}

+(NSString *)WS_SEARCH_REPOSITORIES {
    return @"search/repositories";
}

+(NSString *)WS_PULL_REQUESTS_BY_REPOSITORYWithAuthor:(NSString *)author repositoryName:(NSString *)repositoryName {
    
    NSAssert( author, @"Needs author as parameter" );
    NSAssert( repositoryName, @"Needs repositoryName as parameter" );
    
    return [NSString stringWithFormat:@"repos/%@/%@/pulls", author, repositoryName];
    
}

@end
