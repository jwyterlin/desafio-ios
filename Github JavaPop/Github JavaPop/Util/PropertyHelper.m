//
//  PropertyHelper.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 01/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "PropertyHelper.h"

#import <objc/runtime.h>

@implementation PropertyHelper

+(BOOL)checkProperty:(const char *)property forClass:(Class)class isEqualType:(NSString *)type {
    
    objc_property_t theProperty = class_getProperty( [class class], property );
    
    if ( theProperty == NULL )
        return NO;
    
    const char * propertyAttrs = property_getAttributes(theProperty);
    
    NSString *propString = [NSString stringWithUTF8String:propertyAttrs];
    NSArray *attrArray = [propString componentsSeparatedByString:@","];
    
    NSString *typeString = attrArray[0];
    
    typeString = [typeString stringByReplacingOccurrencesOfString:@"T@\"" withString:@""];
    typeString = [typeString stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    
    if ( [type isEqualToString:@"BOOL"] )
        return [typeString isEqualToString:@"Tc"] || [typeString isEqualToString:@"TB"];
    else if ( [type isEqualToString:@"NSUInteger"] )
        return [typeString isEqualToString:@"TQ"] || [typeString isEqualToString:@"TI"];
    
    return [typeString isEqualToString:type];
    
}

@end
