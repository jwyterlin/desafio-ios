//
//  Constants.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 01/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject

// Cell Nib Names
extern NSString *const kNibNameRepositoryCell;
extern NSString *const kNibNamePullRequestCell;
extern NSString *const kNibNameLoadingMoreCell;

// Error Domain
extern NSString *const RepositoryServiceErrorDomain;
extern NSString *const PullRequestServiceErrorDomain;

// Repository Service Errors
extern const NSInteger RepositoryServiceErrorInvalidRepositoriesJSONDictionary;

// Pull Request Service Errors
extern const NSInteger PullRequestServiceErrorInvalidRepositoriesJSONDictionary;

extern const NSInteger LimitPullRequestsPerPage;

// Notifications
extern NSString *const kNotificationApplicationDidBecomeActive;

@end
