//
//  NetAPIClient.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 02/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@interface NetAPIClient : AFHTTPSessionManager

+(instancetype)sharedClient;

@end
