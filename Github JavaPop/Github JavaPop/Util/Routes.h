//
//  Routes.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 02/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Routes : NSObject

+(NSString *)BASE_URL;
+(NSString *)BASE_URL_API;

+(NSString *)WS_SEARCH_REPOSITORIES;
+(NSString *)WS_PULL_REQUESTS_BY_REPOSITORYWithAuthor:(NSString *)author repositoryName:(NSString *)repositoryName;

@end
