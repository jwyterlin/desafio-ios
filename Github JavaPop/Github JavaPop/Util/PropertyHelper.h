//
//  PropertyHelper.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 01/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PropertyHelper : NSObject

+(BOOL)checkProperty:(const char *)property forClass:(Class)class isEqualType:(NSString *)type;

@end
