//
//  Alert.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 09/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

extern NSString *const kAlertButtonStyleDefault;
extern NSString *const kAlertButtonStyleDestructive;
extern NSString *const kAlertButtonStyleCancel;

@interface Alert : NSObject

-(UIAlertController *)showAlertMessage:(NSString *)message
                        viewController:(UIViewController *)viewController;

-(UIAlertController *)showAlertMessage:(NSString *)message
                        viewController:(UIViewController *)viewController
                        dismissAfterOK:(BOOL)dismissAfterOK;

-(UIAlertController *)showAlertMessage:(NSString *)message
                        viewController:(UIViewController *)viewController
                        dismissAfterOK:(BOOL)dismissAfterOK
                              animated:(BOOL)animated;

-(UIAlertController *)showAlertTitle:(NSString *)title
                             message:(NSString *)message
                      viewController:(UIViewController *)viewController
                      dismissAfterOK:(BOOL)dismissAfterOK
                            animated:(BOOL)animated;

-(void)showError:(NSError *)error viewController:(UIViewController *)viewController;

-(void)showNoConnectionWithViewController:(UIViewController *)viewController;

-(void)showWithTitle:(NSString *)title message:(NSString *)message buttons:(NSArray *)buttons viewController:(UIViewController *)viewController tapBlock:(void(^)(NSInteger buttonIndex))tapBlock;

-(void)showWithTitle:(NSString *)title message:(NSString *)message buttons:(NSArray<NSString *> *)buttons buttonStyles:(NSArray<NSString *> *)buttonStyles viewController:(UIViewController *)viewController tapBlock:(void(^)(NSInteger buttonIndex))tapBlock;

@end

