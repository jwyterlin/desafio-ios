//
//  Alert.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 09/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "Alert.h"

NSString *const kAlertButtonStyleDefault = @"Default";
NSString *const kAlertButtonStyleDestructive = @"Destructive";
NSString *const kAlertButtonStyleCancel = @"Cancel";

@implementation Alert

#pragma mark - Public methods

-(UIAlertController *)showAlertMessage:(NSString *)message viewController:(UIViewController *)viewController {
    
    return [self showAlertMessage:message viewController:viewController dismissAfterOK:NO];
    
}

-(UIAlertController *)showAlertMessage:(NSString *)message viewController:(UIViewController *)viewController dismissAfterOK:(BOOL)dismissAfterOK {
    
    return [self showAlertMessage:message viewController:viewController dismissAfterOK:dismissAfterOK animated:YES];
    
}

-(UIAlertController *)showAlertMessage:(NSString *)message
                        viewController:(UIViewController *)viewController
                        dismissAfterOK:(BOOL)dismissAfterOK
                              animated:(BOOL)animated {
    
    return [self showAlertTitle:nil message:message viewController:viewController dismissAfterOK:dismissAfterOK animated:animated];
    
}

-(UIAlertController *)showAlertTitle:(NSString *)title
                             message:(NSString *)message
                      viewController:(UIViewController *)viewController
                      dismissAfterOK:(BOOL)dismissAfterOK
                            animated:(BOOL)animated {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                 style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   
                                                   if ( dismissAfterOK ) {
                                                       
                                                       if ( viewController.navigationController ) {
                                                           [viewController.navigationController popViewControllerAnimated:animated];
                                                       } else {
                                                           [viewController dismissViewControllerAnimated:animated completion:nil];
                                                       }
                                                       
                                                   }
                                                   
                                               }];
    
    [alert addAction:ok];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [viewController presentViewController:alert animated:YES completion:nil];
    });
    
    return alert;
    
}

-(void)showError:(NSError *)error viewController:(UIViewController *)viewController {
    
    NSString *message = [NSString stringWithFormat:NSLocalizedString(@"An error occurred: %@\n\nFeel free to let us know about this via email contact@email.com", nil),error.localizedDescription];
    
    [self showAlertMessage:message viewController:viewController];
    
}

-(void)showNoConnectionWithViewController:(UIViewController *)viewController {
    
    [self showAlertMessage:NSLocalizedString(@"No internet connection", nil) viewController:viewController];
    
}

-(void)showWithTitle:(NSString *)title message:(NSString *)message buttons:(NSArray *)buttons viewController:(UIViewController *)viewController tapBlock:(void(^)(NSInteger buttonIndex))tapBlock {
    
    [self showWithTitle:title message:message buttons:buttons buttonStyles:nil viewController:viewController tapBlock:tapBlock];
    
}

-(void)showWithTitle:(NSString *)title message:(NSString *)message buttons:(NSArray<NSString *> *)buttons buttonStyles:(NSArray<NSString *> *)buttonStyles viewController:(UIViewController *)viewController tapBlock:(void(^)(NSInteger buttonIndex))tapBlock {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    NSInteger index = 0;
    
    for ( NSString *buttonName in buttons ) {
        
        UIAlertActionStyle style = [self styleByButtonAtIndex:index buttonStyles:buttonStyles];
        
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:buttonName style:style handler:^(UIAlertAction * action) {
            
            if ( tapBlock )
                tapBlock( index );
            
        }];
        
        [alert addAction:alertAction];
        
        index++;
        
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [viewController presentViewController:alert animated:YES completion:nil];
    });
    
}

#pragma mark - Private methods

-(UIAlertActionStyle)styleByButtonAtIndex:(NSInteger)index buttonStyles:(NSArray<NSString *> *)buttonStyles {
    
    if ( ! index )
        return UIAlertActionStyleDefault;
    
    if ( ! buttonStyles )
        return UIAlertActionStyleDefault;
    
    if ( buttonStyles.count == 0 )
        return UIAlertActionStyleDefault;
    
    if ( buttonStyles.count > index ) {
        
        NSString *style = buttonStyles[index];
        
        if ( [style isEqualToString:kAlertButtonStyleDefault] )
            return UIAlertActionStyleDefault;
        else if ( [style isEqualToString:kAlertButtonStyleDestructive] )
            return UIAlertActionStyleDestructive;
        else if ( [style isEqualToString:kAlertButtonStyleCancel] )
            return UIAlertActionStyleCancel;
        
    }
    
    return UIAlertActionStyleDefault;
    
}

@end

