//
//  DeviceInfo.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 02/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "DeviceInfo.h"

@implementation DeviceInfo

+(CGFloat)height {
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    return screenBounds.size.height;
    
}

+(CGFloat)width {
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    return screenBounds.size.width;
    
}

@end
