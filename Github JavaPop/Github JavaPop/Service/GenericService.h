//
//  GenericService.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 01/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import <Foundation/Foundation.h>

// Util
#import "Connection.h"
#import "Constants.h"
#import "Routes.h"

@interface GenericService : NSObject

@end
