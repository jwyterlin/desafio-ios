//
//  RepositoryService.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 01/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "GenericService.h"

// Model
#import "RepositoryModel.h"

@interface RepositoryService : GenericService

//-------------------------------------------------------------------------------------------------------//
//
// Repositories
//
//-------------------------------------------------------------------------------------------------------//

-(void)repositoriesWithPage:(NSNumber *)page
                 completion:(void(^)(NSArray<RepositoryModel *> *repositories))completion
                    failure:(void(^)(BOOL hasNoConnection, NSError *error))failure;

-(void)repositoriesWithPage:(NSNumber *)page
                 completion:(void(^)(NSArray<RepositoryModel *> *repositories))completion
                    failure:(void(^)(BOOL hasNoConnection, NSError *error))failure
                       test:(void(^)(id responseData, NSError *error))test;

@end
