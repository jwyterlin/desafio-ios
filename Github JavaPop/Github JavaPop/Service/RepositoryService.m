//
//  RepositoryService.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 01/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "RepositoryService.h"

#import "RepositoryModel.h"

@implementation RepositoryService

//-------------------------------------------------------------------------------------------------------//
//
// Repositories
//
//-------------------------------------------------------------------------------------------------------//

-(void)repositoriesWithPage:(NSNumber *)page
                 completion:(void(^)(NSArray<RepositoryModel *> *repositories))completion
                    failure:(void(^)(BOOL hasNoConnection, NSError *error))failure {
    
    [self repositoriesWithPage:page completion:completion failure:failure test:^(id responseData, NSError *error) {
        // Do nothing
    }];
    
}

-(void)repositoriesWithPage:(NSNumber *)page
                 completion:(void(^)(NSArray<RepositoryModel *> *repositories))completion
                    failure:(void(^)(BOOL hasNoConnection, NSError *error))failure
                       test:(void(^)(id responseData, NSError *error))test {
    
    NSDictionary *parameters = @{
                                 @"q":@"language:Java",
                                 @"sort":@"stars",
                                 @"page":page
                                };
    
    Connection *con = [Connection new];
    [con connectWithMethod:RequestMethodGet requestSerializer:RequestSerializerJSON url:[Routes WS_SEARCH_REPOSITORIES] parameters:parameters success:^(id responseData) {
        
        if ( test )
            test( responseData, nil );
        
        NSDictionary *result = (NSDictionary *)responseData;
        
        NSError *error;
        
        if ( result[@"items"] ) {
            
            NSArray *items = result[@"items"];
            
            NSArray *repositories = [MTLJSONAdapter modelsOfClass:[RepositoryModel class] fromJSONArray:items error:&error];
            
            if ( error ) {
                if ( failure )
                    failure( NO, error );
                return;
            }
            
            if ( completion )
                completion( repositories );
            return;
            
        }
        
        error = [NSError errorWithDomain:RepositoryServiceErrorDomain
                                    code:RepositoryServiceErrorInvalidRepositoriesJSONDictionary
                                userInfo:@{NSLocalizedDescriptionKey:NSLocalizedString(@"Failed to receive data.", nil)}];
        
        if ( failure )
            failure( NO, error );
        
    } failure:^(BOOL hasNoConnection, NSError *error) {
        
        if ( test )
            test( nil, error );
        
        if ( failure )
            failure( hasNoConnection, error );
        
    }];

}

@end
