//
//  PullRequestService.h
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 02/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "GenericService.h"

// Model
#import "PullRequestModel.h"

@interface PullRequestService : GenericService

//-------------------------------------------------------------------------------------------------------//
//
// Pull requests by repository
//
//-------------------------------------------------------------------------------------------------------//

-(void)pullRequestsWithAuthor:(NSString *)author
               repositoryName:(NSString *)repositoryName
                         page:(NSNumber *)page
                   completion:(void(^)(NSArray<PullRequestModel *> *pullRequests))completion
                      failure:(void(^)(BOOL hasNoConnection, NSError *error))failure;

-(void)pullRequestsWithAuthor:(NSString *)author
               repositoryName:(NSString *)repositoryName
                         page:(NSNumber *)page
                   completion:(void(^)(NSArray<PullRequestModel *> *pullRequests))completion
                      failure:(void(^)(BOOL hasNoConnection, NSError *error))failure
                         test:(void(^)(id responseData, NSError *error))test;

@end
