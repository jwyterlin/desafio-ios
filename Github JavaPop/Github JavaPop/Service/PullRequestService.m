//
//  PullRequestService.m
//  Github JavaPop
//
//  Created by Jhonathan Wyterlin on 02/11/16.
//  Copyright © 2016 Jhonathan Wyterlin. All rights reserved.
//

#import "PullRequestService.h"

@implementation PullRequestService

//-------------------------------------------------------------------------------------------------------//
//
// Pull requests by repository
//
//-------------------------------------------------------------------------------------------------------//

-(void)pullRequestsWithAuthor:(NSString *)author
               repositoryName:(NSString *)repositoryName
                         page:(NSNumber *)page
                   completion:(void(^)(NSArray<PullRequestModel *> *pullRequests))completion
                      failure:(void(^)(BOOL hasNoConnection, NSError *error))failure {
    
    [self pullRequestsWithAuthor:author repositoryName:repositoryName page:page completion:completion failure:failure test:^(id responseData, NSError *error) {
       // Do nothing
    }];
    
}

-(void)pullRequestsWithAuthor:(NSString *)author
               repositoryName:(NSString *)repositoryName
                         page:(NSNumber *)page
                   completion:(void(^)(NSArray<PullRequestModel *> *pullRequests))completion
                      failure:(void(^)(BOOL hasNoConnection, NSError *error))failure
                         test:(void(^)(id responseData, NSError *error))test {
    
    NSDictionary *parameters = @{
                                 @"page":page,
                                 @"state":@"all"
                                };
    
    NSString *urlString = [Routes WS_PULL_REQUESTS_BY_REPOSITORYWithAuthor:author repositoryName:repositoryName];
    
    Connection *con = [Connection new];
    [con connectWithMethod:RequestMethodGet requestSerializer:RequestSerializerJSON url:urlString parameters:parameters success:^(id responseData) {
        
        if ( test )
            test( responseData, nil );
        
        NSArray *result = (NSArray *)responseData;
        
        NSError *error;
        
        if ( result ) {
            if ( result.count == 0 ) {
                if ( completion )
                    completion( result );
                return;
            }
        } else {
            
            error = [NSError errorWithDomain:PullRequestServiceErrorDomain
                                        code:PullRequestServiceErrorInvalidRepositoriesJSONDictionary
                                    userInfo:@{NSLocalizedDescriptionKey:NSLocalizedString(@"Failed to receive data.", nil)}];
            
            if ( failure )
                failure( NO, error );
            return;
            
        }
            
        NSArray *pullRequests = [MTLJSONAdapter modelsOfClass:[PullRequestModel class] fromJSONArray:result error:&error];
        
        if ( error ) {
            if ( failure )
                failure( NO, error );
            return;
        }
        
        if ( completion )
            completion( pullRequests );
        
    } failure:^(BOOL hasNoConnection, NSError *error) {
        
        if ( test )
            test( nil, error );
        
        if ( failure )
            failure( hasNoConnection, error );
        
    }];
    
}

@end
